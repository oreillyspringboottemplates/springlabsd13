package com.oreillyauto.model.groupkt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
    private String name;
    
    @JsonProperty("alpha2_code")
    private String alphaTwoCode;
    
    @JsonProperty("alpha3_code")
    private String alphaThreeCode;

    public Result() {}

    public Result(String name, String alphaTwoCode, String alphaThreeCode) {
		super();
		this.name = name;
		this.alphaTwoCode = alphaTwoCode;
		this.alphaThreeCode = alphaThreeCode;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getAlphaTwoCode() {
		return alphaTwoCode;
	}

	public void setAlphaTwoCode(String alphaTwoCode) {
		this.alphaTwoCode = alphaTwoCode;
	}

	public String getAlphaThreeCode() {
		return alphaThreeCode;
	}

	public void setAlphaThreeCode(String alphaThreeCode) {
		this.alphaThreeCode = alphaThreeCode;
	}

	@Override
	public String toString() {
		return "Result [name=" + name + ", alphaTwoCode=" + alphaTwoCode + ", alphaThreeCode=" + alphaThreeCode + "]";
	}

}
