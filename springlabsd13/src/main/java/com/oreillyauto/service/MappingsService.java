package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.interns.Intern;

public interface MappingsService {
	public List<Intern> getInterns();
}
