package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.InternRepository;
import com.oreillyauto.domain.interns.Intern;
import com.oreillyauto.service.MappingsService;

@Service("mappingsService")
public class MappingsServiceImpl implements MappingsService {
    
	@Autowired
    InternRepository internRepo;
    
	@Override
	public List<Intern> getInterns() {
		return (List<Intern>)internRepo.findAll();
	}
    
}
