<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Email Us</h1>
<div id="carPartMessage"></div>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-12">
				<form method="post" id="contactUsForm" action="<c:url value='/carparts/contactus' />">
					<div class="form-group col-sm-12">
						<div class="col-sm-5 form-group">
							<label for="email">From E-mail Address</label>
							<orly-input id="emailAddress" name="emailAddress" placeholder="E-mail Address"></orly-input>
						</div>
						<div class="col-sm-12 form-group">
							<label for="emailBody" class="mt10">Email Body</label>
							<orly-text id="emailBody" name="emailBody" placeholder="Enter your message"></orly-text>
						</div>
						<div class="col-sm-3 form-group">
							<!-- <button id="submitBtn" type="button" class="btn btn-primary">Submit</button> -->
							<orly-btn id="submitBtn" spinonclick text="Submit"></orly-btn>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
orly.ready.then (()=> {
	// Variable Declarations
	const submitBtn = orly.qid("submitBtn");
	
	// submitBtn Event
	orly.qid("submitBtn").addEventListener("click", function(e){
		try {
			e.preventDefault(); // Prevent Form From Submitting "normally" (IE)
			let data = {};
			data.emailAddress = orly.qid("emailAddress").value;
			data.emailBody = orly.qid("emailBody").value;
						
			// Make AJAX call to the server along with a message
			fetch("<c:url value='/carparts/emailus' />", {
			        method: "POST",
			        body: JSON.stringify(data),
			        headers: {
			            "Content-Type": "application/json"
			        }
			}).then(function(response) { // <===== HttpServletResponse Object
				// Get the response, grab the JSON, and return response so we
				// can process it in the next "then" (since this is a Promise...)
			  	if (response.ok) {
			  		console.log("response.status=", response.status);
			  		let message = response.json();
			  		console.log("message = " + message);
			  		return message;
			  	}
			}).then(function(response) {
				stopSpinningButton(submitBtn);
				let Email = JSON.parse(response);
				let message = Email.message;
				let messageType = Email.messageType;
				
				if (messageType == null || messageType == "undefined" || messageType.length == 0) {
					messageType = "info";
				}
			  	
			  	if (message != null && message != "undefined" && message.length > 0) {
			  		orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});	
			  	}
			}).catch(function(error) {
				stopSpinningButton(submitBtn);
				let message = 'There was a problem with your fetch operation: ' + error.message;
				orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
			});
	
		} catch (err) {
			stopSpinningButton(submitBtn);
			// Do not show try-catch errors to the user in production
			orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:err});
		}
	});
}); // end orly.ready.then

// Stop spinning O'Reilly submit button in the form
function stopSpinningButton(button) {
	try {
		button.stopSpinning();
	} catch (err) {
		console.log("stopSpinningButton() Error: " + err);
	}
}
</script>
