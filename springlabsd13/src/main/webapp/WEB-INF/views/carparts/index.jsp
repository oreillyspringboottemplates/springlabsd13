<%@ include file="/WEB-INF/layouts/include.jsp"%>
<div>
	<h1>Car Parts - 1994 Pontiac Firebird</h1>
	<div class="row">
		<div class="col-sm-9">
			<div class="text-danger">${error}</div>
			<strong>Part Number:</strong> ${carpart.partNumber}<br/>
			<strong>Line:</strong> ${carpart.line}<br/>
			<strong>Description:</strong> ${carpart.description}<br/><br/>
			<strong>DETAILS</strong><br/>
			<hr class="squeeze"/>
			<c:forEach items="${carpart.detailsMap}" var="entry">
				<strong><c:out value="${entry.key}" /></strong> : <c:out value="${entry.value}" />
				<br/>
			</c:forEach>
		    <br/>
			<strong>Store Locations:</strong><br/>
<%-- 			<c:forEach items="${carpart.storeLocations}" var="element">
				<c:out value="${element}" escapeXml="false" />
			</c:forEach> --%>

			<c:if test="${not empty carpart.storeLocations}">
			    <c:forEach items="${carpart.storeLocations}" var="storeLocation">
			    	<%-- ${element}<br/> --%>
			        ${storeLocation.storeNumber} - ${storeLocation.storeDetails}<br/>
			    </c:forEach>
			</c:if>
		</div>
		<div class="col-sm-3">
			<c:if test="${not empty carpart.imageName}">
				<img class="img-fluid" alt="car part image"  
				       src="<c:url value='/resources/img/carparts/${carpart.imageName}'/>" />
			</c:if>
			<c:if test="${empty carpart.imageName}">
				No Image Found
			</c:if>
		</div>
	</div>
</div>
