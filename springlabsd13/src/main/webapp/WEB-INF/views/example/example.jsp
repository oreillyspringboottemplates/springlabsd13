<%@ include file="/WEB-INF/layouts/include.jsp"%>

<orly-header title="Awesome App">
  <orly-nav-item label="F.A.Q" name="hyper" href="#"></orly-nav-item>
</orly-header>
<br/><br/>
<h1>Example JSP!</h1>

<h2>${anyOldString}</h2>

<!-- JSTL -->
<c:if test='${anyOldString == "anything"}'> 
    <b>anyOldString = &quot;anything&quot;!</b> 
</c:if>


<c:if test="${not empty errorMessage}">
	<p class="danger"><b>Error: </b>${errorMessage}</p>
</c:if>

<b>Result</b>: ${result}
<br/>

<orly-leftnav active="search">
  <orly-nav-item label="Dashboard" name="home" href="#home" icon="home"></orly-nav-item>
  <orly-nav-item label="Search" name="search" href="#search" icon="search"></orly-nav-item>
</orly-leftnav>
<div class="row">
  <div class="col-sm-3 form-group">
    <label for="">Team Member</label>
    <orly-input placeholder="Name or Number"></orly-input>
  </div>
  <div class="col-sm-3 form-group">
    <label for="storeNumber">Store #</label>
    <orly-input id="storeNumber" placeholder="Store #"></orly-input>
  </div>
  <div class="col-sm-2 form-group">
  	<label class="block" for="myBtn">&nbsp;</label>
    <button id="myBtn" name="myBtn" class="float-right btn btn-info btn-nl">Search</button>
  </div>
</div>
<hr>
<orly-table loaddataoncreate includefilter maxrows="3" tabletitle="Search Results" class="invisible" url="<c:url value='/resources/assets/MOCK_DATA_LARGE.json' />">
  <orly-column field="id" label="Id" class="" sorttype="natural"></orly-column>
  <orly-column name="fullname" label="Full Name" class="">
    <div slot="cell">
      \${{html: table.renderCellText(model.first_name, table.filter)}}
      \${{html: table.renderCellText(model.last_name, table.filter)}}
    </div>
  </orly-column>
  <orly-column field="email" label="Email"></orly-column>
</orly-table>
<orly-footer>
  <orly-nav-item label="Add Item" name="new" href="#"></orly-nav-item>
  <orly-nav-item label="Add Type" name="add" href="#"></orly-nav-item>
</orly-footer>

