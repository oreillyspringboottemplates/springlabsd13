<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>User</h1>
<h4>My User Page</h4>
<div class="card">
	<div class="card-body">
		<%-- ${user} --%>
		<%-- <fmt:formatDate value="${dateValue}" pattern="MM/dd/yyyy HH:mm"/> --%>
		<!-- orly.formatDate(new Date(item.birthDate), 'MM/DD/YYYY') -->
		
		<form method="post" id="contactUsForm" action="<c:url value='/users/userPage' />">
			<div class="form-group col-sm-12">
				<div class="col-sm-5 form-group">
					<label for="email">User ID</label>
					<orly-input id="userId" name="userId" value="${user.userId}" placeholder="User ID" disabled></orly-input>
				</div>
				<div class="col-sm-12 form-group">
					<label for="firstname" class="mt10">First Name</label>
					<orly-text id="firstName" name="firstName" value="${user.firstName}" placeholder="First Name"></orly-text>
				</div>
				<div class="col-sm-12 form-group">
					<label for="lastName" class="mt10">Last Name</label>
					<orly-text id="lastName" name="lastName" value="${user.lastName}" placeholder="Last Name"></orly-text>
				</div>
				<div class="col-sm-12 form-group">
					<label for="department" class="mt10">Department</label>
					<orly-text id="department" name="department" value="${user.department}" placeholder="Department"></orly-text>
				</div>
				<div class="col-sm-12 form-group">
					<label for="birthDate" class="mt10">Birth Date</label>
					<orly-datepicker id="birthDateString" name="birthDateString" 
					                 value="${user.birthDate}" size="sm"></orly-datepicker>
				</div>
				<div class="col-sm-12 form-group">
					<label for="role" class="mt10">Role</label>
				    <orly-input required placeholder="Options" id="role" name="role">	
						<c:forEach items="${roles}" var="item">
							<orly-option label="${item.role}" value="${item.id}"></orly-option>
						</c:forEach>
				    </orly-input>
				</div>
				<div class="col-sm-3 form-group">
					<button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
					<!-- <orly-btn id="submitBtn" spinonclick text="Submit"></orly-btn> -->
				</div>
			</div>
		</form>
<!--     private Integer userId;
		
	@Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;
	
	@Column(name = "last_name", columnDefinition = "VARCHAR(256)")
    private String lastName;
	
	@Column(name = "department", columnDefinition = "VARCHAR(256)")
    private String department;
	
	@Column(name = "birth_date", columnDefinition = "TIMESTAMP")
    private Timestamp birthDate; -->
		
	</div>
</div>

<script>	
	orly.ready.then(() => {
	});
</script>
